package com.jnuneslab.stockholder.data;

import android.net.Uri;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;



/**
 * Matches content provider
 */
public class QuoteProvider extends ContentProvider {

    // Log tag
    private static final String TAG = QuoteProvider.class.getSimpleName();

    // URI Matcher
    private UriMatcher mUriMatcher = buildUriMatcher();
    private static StockDBHelper mOpenHelper;

    private static final int QUOTES = 100;

    // match_id = ?
    private static final String QUOTE_BY_ID = StockContract.QuoteEntry.COLUMN_QUOTE_ID + " = ?";

    /**
     *  UriMatcher match each URI to the LEAGUE, MATCH_ID, MATCH_DATE
     */
    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = StockContract.BASE_CONTENT_URI.toString();

        // Create a corresponding code for each type of URI
        matcher.addURI(authority, null, QUOTES);
       // matcher.addURI(authority, "leagues", LEAGUES);
      //  matcher.addURI(authority, "league", MATCHES_WITH_LEAGUE);
      //  matcher.addURI(authority, "id", MATCHES_WITH_ID);
     //   matcher.addURI(authority, "date", MATCHES_WITH_DATE);

        return matcher;
    }
/** REFACTORY HERE **/
    private int matchURI(Uri uri) {
        String link = uri.toString();
        if (link.contentEquals(StockContract.BASE_CONTENT_URI.toString())) {
            return QUOTES;
        }else  if (link.contentEquals(StockContract.QuoteEntry.CONTENT_URI.toString())) {
            return QUOTES;
        }/*else if (link.contentEquals(MatchesContract.LeagueEntry.buildLeague().toString())) {
            return LEAGUES;
        }else if (link.contentEquals(MatchesContract.MatchesEntry.buildScoreWithDate().toString())) {
            return MATCHES_WITH_DATE;
        } else if (link.contentEquals(MatchesContract.MatchesEntry.buildScoreWithId().toString())) {
            return MATCHES_WITH_ID;
        } else if (link.contentEquals(MatchesContract.MatchesEntry.buildScoreWithLeague().toString())) {
            return MATCHES_WITH_LEAGUE;
        }*/


        return -1;
    }

    @Override
    public boolean onCreate() {

        // Create Matches DB Helper
        mOpenHelper = new StockDBHelper(getContext());
        return false;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = mUriMatcher.match(uri);
        int rowsUpdated;

       // switch (match) {
      //      case QUOTES:
                rowsUpdated = db.update(StockContract.QuoteEntry.TABLE_NAME, values, selection,
                        selectionArgs);
     //           break;
     //       default:
    //            throw new UnsupportedOperationException("Unknown uri: " + uri);
    //    }
        if (rowsUpdated != 0) {
            Log.w(TAG, ">>> row updated>" + rowsUpdated + values.get(StockContract.QuoteEntry.COLUMN_IS_CURRENT) +"//" + values.get(StockContract.QuoteEntry.COLUMN_CHANGE));
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        final int match = mUriMatcher.match(uri);
        switch (match) {
            case QUOTES:
                return StockContract.QuoteEntry.CONTENT_TYPE;
            /*case LEAGUES:
                return MatchesContract.LeagueEntry.CONTENT_TYPE;
            case MATCHES_WITH_LEAGUE:
                return MatchesContract.MatchesEntry.CONTENT_TYPE;
            case MATCHES_WITH_ID:
                return MatchesContract.MatchesEntry.CONTENT_ITEM_TYPE;
            case MATCHES_WITH_DATE:
                return MatchesContract.MatchesEntry.CONTENT_TYPE;*/
            default:
                throw new UnsupportedOperationException("Unknown uri :" + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor retCursor;
        int match = matchURI(uri);

        switch (match) {
            case QUOTES:
                retCursor = db.query(
                        StockContract.QuoteEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
          /*  case LEAGUES:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        MatchesContract.LeagueEntry.TABLE_NAME,
                        projection, null, selectionArgs, null, null, sortOrder);
                break;
            case MATCHES_WITH_DATE:
                sortOrder = MatchesContract.LeagueEntry.COLUMN_LEAGUE_NAME + " DESC";
                retCursor = sMatcheByLeagueQueryBuilder.query(
                        mOpenHelper.getReadableDatabase(),
                        projection, SCORES_BY_DATE, selectionArgs, null, null, sortOrder);
                break;
            case MATCHES_WITH_ID:
                sortOrder = MatchesContract.LeagueEntry.COLUMN_LEAGUE_NAME + " DESC";
                retCursor = sMatcheByLeagueQueryBuilder.query(
                        mOpenHelper.getReadableDatabase(),
                        projection, SCORES_BY_ID, selectionArgs, null, null, sortOrder);
                break;
            case MATCHES_WITH_LEAGUE:
                sortOrder = MatchesContract.LeagueEntry.COLUMN_LEAGUE_NAME + " DESC";
                retCursor = sMatcheByLeagueQueryBuilder.query(
                        mOpenHelper.getReadableDatabase(),
                        projection, SCORES_BY_LEAGUE, selectionArgs, null, null, sortOrder);
                break;*/
            default:
                throw new UnsupportedOperationException("Unknown Uri" + uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {

        // Only use bulkInsert
        return null;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, ContentValues[] values) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (matchURI(uri)) {
            case QUOTES:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(StockContract.QuoteEntry.TABLE_NAME, null, value,
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                Log.w(TAG, uri.toString());
                return super.bulkInsert(uri, values);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int rowsDeleted;

        // Delete all rows
        switch (matchURI(uri)) {
            case QUOTES:

               rowsDeleted = db.delete(StockContract.QuoteEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:

           throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }
}