package com.jnuneslab.stockholder.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * DB helper
 * Created by Walter on 27/09/2015.
 */
public class StockDBHelper extends SQLiteOpenHelper {
  static final int DATABASE_VERSION = 2;

  static final String DATABASE_NAME = "stock.db";

  public StockDBHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    final String createQuotesTable = "CREATE TABLE " + StockContract.QuoteEntry.TABLE_NAME + " ( "
            + StockContract.QuoteEntry._ID + " INTEGER PRIMARY KEY, "
            + StockContract.QuoteEntry.COLUMN_QUOTE_ID + ", "
            + StockContract.QuoteEntry.COLUMN_SYMBOL + " TEXT NOT NULL, "
            + StockContract.QuoteEntry.COLUMN_PERCENT_CHANGE + " TEXT NOT NULL, "
            + StockContract.QuoteEntry.COLUMN_CHANGE + " TEXT NOT NULL, "
            + StockContract.QuoteEntry.COLUMN_BID_PRICE + " TEXT NOT NULL, "
            + StockContract.QuoteEntry.COLUMN_CREATED + " TEXT, "
            + StockContract.QuoteEntry.COLUMN_IS_UP + " INTEGER NOT NULL, "
            + StockContract.QuoteEntry.COLUMN_IS_CURRENT + " INTEGER NOT NULL, "
            + StockContract.QuoteEntry.COLUMN_CLOSE_HISTORICAL_ONE_MONTH + " TEXT, "
            + StockContract.QuoteEntry.COLUMN_MAX_MIN_ONE_MONTH + " TEXT, "
            + StockContract.QuoteEntry.COLUMN_NAME + " TEXT, "
            + StockContract.QuoteEntry.COLUMN_STOCK_NAME + " TEXT, "
            + StockContract.QuoteEntry.COLUMN_DAY_HIGH + " TEXT, "
            + StockContract.QuoteEntry.COLUMN_DAY_LOW + " TEXT, "
            + " UNIQUE (" + StockContract.QuoteEntry.COLUMN_SYMBOL + ") ON CONFLICT REPLACE);";

    db.execSQL(createQuotesTable);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + StockContract.QuoteEntry.TABLE_NAME);
    onCreate(db);
  }

}