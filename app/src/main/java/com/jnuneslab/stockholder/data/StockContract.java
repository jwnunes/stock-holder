package com.jnuneslab.stockholder.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Database Movie contract
 * Created by Walter on 27/09/2015.
 */
public abstract class StockContract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "com.jnuneslab.stockholder";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final Uri BASE_CONTENT_URI2 = Uri.parse("content://" + CONTENT_AUTHORITY + ".data.QuoteProvider");
    // Possible paths (appended to base content URI for possible URI's)
     public static final String PATH_QUOTE = "quote";

    /* Inner class that defines the table contents of the quote table */
    public static final class QuoteEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_QUOTE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_QUOTE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_QUOTE;

        public static final String TABLE_NAME = "quote";

        // Columns
        public static final String COLUMN_QUOTE_ID = "quote_id"; // may be not used
        public static final String COLUMN_SYMBOL = "symbol";
        public static final String COLUMN_PERCENT_CHANGE = "percent_change";
        public static final String COLUMN_CHANGE = "change";
        public static final String COLUMN_BID_PRICE = "bid_price";
        public static final String COLUMN_CREATED = "created";
        public static final String COLUMN_IS_UP = "is_up";
        public static final String COLUMN_IS_CURRENT = "is_current";
        public static final String COLUMN_CLOSE_HISTORICAL_ONE_MONTH = "close_historical_one_month";
        public static final String COLUMN_MAX_MIN_ONE_MONTH = "max_min_one_month";
        public static final String COLUMN_NAME = "full_name";
        public static final String COLUMN_STOCK_NAME = "stock_name";
        public static final String COLUMN_DAY_HIGH = "day_high";
        public static final String COLUMN_DAY_LOW = "day_low";


        public static Uri buildMovieIdUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildMoviePosterURI(String posterPath){
            return CONTENT_URI.buildUpon().appendPath(posterPath.substring(1)).build(); // Remove the first slash of the URL
        }

        public static String getPosterFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static long getIdFromUri(Uri uri) {
            return ContentUris.parseId(uri);
        }

        public static Uri buildMovieWithId(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

}