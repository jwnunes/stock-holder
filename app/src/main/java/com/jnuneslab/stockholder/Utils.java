package com.jnuneslab.stockholder;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.util.Log;


import com.jnuneslab.stockholder.data.StockContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sam_chordas on 10/8/15.
 */
public class Utils {

  private static String LOG_TAG = Utils.class.getSimpleName();

  public static boolean showPercent = true;

  public static ArrayList<ContentValues> quoteJsonToContentVals(String JSON){
    ArrayList<ContentValues> batchOperations = new ArrayList<>();
    JSONObject jsonObject = null;
    JSONArray resultsArray = null;
    ContentValues batchResult;
    Log.i(LOG_TAG, "GET FB: " +JSON);
    try{
      jsonObject = new JSONObject(JSON);
      if (jsonObject != null && jsonObject.length() != 0){
        jsonObject = jsonObject.getJSONObject("query");
        int count = Integer.parseInt(jsonObject.getString("count"));
        if (count == 1){
          jsonObject = jsonObject.getJSONObject("results")
                  .getJSONObject("quote");
          batchResult = buildBatchOperation(jsonObject);
          if (batchResult != null)
          batchOperations.add(batchResult);
        } else{
          resultsArray = jsonObject.getJSONObject("results").getJSONArray("quote");

          if (resultsArray != null && resultsArray.length() != 0){
            for (int i = 0; i < resultsArray.length(); i++){
              jsonObject = resultsArray.getJSONObject(i);
              batchResult = buildBatchOperation(jsonObject);
              if (batchResult != null)
              batchOperations.add(batchResult);
            }
          }
        }
      }
    } catch (JSONException e){
      Log.e(LOG_TAG, "String to JSON failed: " + e);
    }
    return batchOperations;
  }

  public static String truncateBidPrice(String bidPrice){
    bidPrice = String.format("%.2f", Float.parseFloat(bidPrice));
    return bidPrice;
  }

  public static String truncateChange(String change, boolean isPercentChange){
//if (change.equals("null")) return "0";
    String weight = change.substring(0, 1);
    String ampersand = "";
    if (isPercentChange){
      ampersand = change.substring(change.length() - 1, change.length());
      change = change.substring(0, change.length() - 1);
    }
    change = change.substring(1, change.length());
    double round = (double) Math.round(Double.parseDouble(change) * 100) / 100;
    change = String.format("%.2f", round);
    StringBuffer changeBuffer = new StringBuffer(change);
    changeBuffer.insert(0, weight);
    changeBuffer.append(ampersand);
    change = changeBuffer.toString();
    return change;
  }


  public static ContentValues buildBatchOperation(JSONObject jsonObject){
    ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(
            StockContract.BASE_CONTENT_URI);

    ContentValues contentValues = new ContentValues();
    try {

      String change = jsonObject.getString("Change");
      if(change.equals("null")) return null;
      if(change.equals("+0.00")) return null;
      System.out.println("><1" + jsonObject.getString("ChangeinPercent"));
      System.out.println("><2" + change);
      System.out.println("><2" + jsonObject.getString("symbol"));
       contentValues.put(StockContract.QuoteEntry.COLUMN_SYMBOL, jsonObject.getString("symbol"));
      contentValues.put(StockContract.QuoteEntry.COLUMN_BID_PRICE, truncateBidPrice(jsonObject.getString("Bid")));

      contentValues.put(StockContract.QuoteEntry.COLUMN_PERCENT_CHANGE, truncateChange(
              jsonObject.getString("ChangeinPercent"), true));
      contentValues.put(StockContract.QuoteEntry.COLUMN_CHANGE, truncateChange(change, false));
      contentValues.put(StockContract.QuoteEntry.COLUMN_IS_CURRENT, 1);
      if (change.charAt(0) == '-'){
        contentValues.put(StockContract.QuoteEntry.COLUMN_IS_UP, 0);
      }else{
        contentValues.put(StockContract.QuoteEntry.COLUMN_IS_UP, 1);
      }
      contentValues.put(StockContract.QuoteEntry.COLUMN_NAME, jsonObject.getString("name"));
      contentValues.put(StockContract.QuoteEntry.COLUMN_STOCK_NAME, jsonObject.getString("StockExchange"));
      contentValues.put(StockContract.QuoteEntry.COLUMN_DAY_HIGH, jsonObject.getString("DaysHigh"));
      contentValues.put(StockContract.QuoteEntry.COLUMN_DAY_LOW, jsonObject.getString("DaysLow"));

    } catch (JSONException e){
      e.printStackTrace();
    }
    return contentValues;
  }


}

