package com.jnuneslab.stockholder;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jnuneslab.stockholder.data.StockContract;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ScrollTabFragment class is responsible to handle the Pager menu (toolbar)
 */
public final class ScrollTabFragment extends Fragment {

    // Total number of tab present in toolbar
    public static final int NUM_PAGES = 3;

    // Start position of toolbar. Start position is zero
    public static final int TODAY_POSITION = 1;

    // State fragment identifier to be add as argument in the fragment
    public static final String STATE_DATE_FRAGMENT = "date_fragment";

     private BlankFragment[] viewFragments = new BlankFragment[NUM_PAGES];
    private ScrollTabAdapter mPagerScrollAdapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scroll_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize variables
        mPagerScrollAdapter = new ScrollTabAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        mViewPager.setAdapter(mPagerScrollAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        for (int i = 0; i < NUM_PAGES; i++) {
            viewFragments[i] = BlankFragment.newInstance("teste" +i,"teste2");
        }

        // Set the listener to be able to change the tabs by clicking in the item directly (without swipe)
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {  }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {  }
        });

        // Set initial values in ViewPager
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mViewPager.setOffscreenPageLimit(NUM_PAGES);
        mViewPager.setAdapter(mPagerScrollAdapter);
        mViewPager.setCurrentItem(TODAY_POSITION);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (FragmentUtil.isConnected(getContext())) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    final EditText edittext = new EditText(getActivity().getApplicationContext());
                    edittext.setTextColor(Color.BLACK); //change
                    //edittext.setPadding(20, 0, 20, 0);
                    edittext.setSingleLine();
                    alert.setMessage("Search for a stock symbol");
                    alert.setTitle("Symbol search");
                    alert.setView(edittext, 70, 0, 70, 0);

                    alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String edtString = edittext.getText().toString();
                            Log.e("graphicLayout", "testep3" + edtString);
                            Cursor c = getContext().getContentResolver().query(StockContract.QuoteEntry.CONTENT_URI,
                                    new String[]{StockContract.QuoteEntry.COLUMN_SYMBOL}, StockContract.QuoteEntry.COLUMN_SYMBOL + "= ?",
                                    new String[]{edtString}, null);
                            if (c.getCount() != 0) {
                                Log.e("graphicLayout", "testep2" + edtString);
                                FragmentUtil.centerToast(getContext(), "This stock is already saved!");

                                return;
                            } else {
                                Log.e("graphicLayout", "testep" + edtString);
                                // Add the stock to DB

                                Intent mServiceIntent = new Intent(getActivity(), StockIntentService.class);
                                mServiceIntent.putExtra("tag", "add");
                                mServiceIntent.putExtra("symbol", edtString.toString());
                                getActivity().startService(mServiceIntent);
                            }


                        }
                    });

                    alert.show();
                } else {
                    FragmentUtil.bottomToast(getContext(), "No Internet connection");
                }


            }
        });
    }

    public void teste(){
        mViewPager.setCurrentItem(TODAY_POSITION);
    }

    /**
     * ScrollTab Adapter class responsible to handle the values that will be inserted in each tab
     */
    private class ScrollTabAdapter extends FragmentStatePagerAdapter {

        /**
         * Constructor
         *
         * @param fm Fragment Mananger
         */
        public ScrollTabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

             return viewFragments[i];
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return getNamePosition(getActivity(), position);
        }

        public String getNamePosition(Context context, int position) {

            switch (position){
                case 0: return "1 month";
                case 1: return "today";
                case 2: return "3 months";
                default: return "unknown";
            }

        }
    }
}
