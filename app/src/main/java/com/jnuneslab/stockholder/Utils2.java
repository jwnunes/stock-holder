package com.jnuneslab.stockholder;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.util.Log;

import com.jnuneslab.stockholder.data.StockContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sam_chordas on 10/8/15.
 */
public class Utils2 {

    private static String LOG_TAG = Utils2.class.getSimpleName();

    public static boolean showPercent = true;

    public static ArrayList<ContentValues> quoteJsonToContentVals(String JSON) {
        ArrayList<ContentValues> batchOperations = new ArrayList<>();
        JSONObject jsonObject = null;
        JSONArray resultsArray = null;
        Float oneMonthMin = 0f;
        Float oneMonthMax = 0f;
        Float closeValue;
        Log.i(LOG_TAG, "GET FB: " + JSON);
        try {
            jsonObject = new JSONObject(JSON);
            if (jsonObject != null && jsonObject.length() != 0) {
                jsonObject = jsonObject.getJSONObject("query");
                int count = Integer.parseInt(jsonObject.getString("count"));


                resultsArray = jsonObject.getJSONObject("results").getJSONArray("quote");

                if (resultsArray != null && resultsArray.length() != 0) {
                    String lastSymbol = "";
                    StringBuilder result = new StringBuilder();
                    for (int i = 0; i < resultsArray.length(); i++) {
                        jsonObject = resultsArray.getJSONObject(i);
                        if (i == 0) {
                            lastSymbol = jsonObject.getString("Symbol");
                            closeValue = Float.parseFloat(jsonObject.getString("Close"));
                            oneMonthMin = closeValue;
                            oneMonthMax = closeValue;
                            result.append(truncateClose(closeValue.toString()));

                        } else if (jsonObject.getString("Symbol").equals(lastSymbol)) {
                            closeValue = Float.parseFloat(jsonObject.getString("Close"));
                            if (closeValue < oneMonthMin)
                                oneMonthMin = closeValue;
                            if (closeValue > oneMonthMax)
                            oneMonthMax = closeValue;
                            result.append("," + truncateClose(closeValue.toString()));

                        } else {
                            //Isert database here
                            batchOperations.add(buildBatchOperation(result.toString(), lastSymbol, oneMonthMin.toString(), oneMonthMax.toString()));
                            System.out.println("><23<" + result);
                            System.out.println("><23<" + oneMonthMin + "-" + oneMonthMax);
                            lastSymbol = jsonObject.getString("Symbol");
                            result = new StringBuilder();
                            oneMonthMin = 0f;
                            oneMonthMax = 0f;
                            closeValue = Float.parseFloat(jsonObject.getString("Close"));

                                oneMonthMin = closeValue;
                            if (closeValue > oneMonthMax)
                                oneMonthMax = closeValue + 0.1f;
                            result.append(truncateClose(closeValue.toString()));
                        }
                        // batchOperations.add(buildBatchOperation(jsonObject));
                    }
                    //Isert last value database here
                    System.out.println("><23<" + result);
                    System.out.println("><23<" + oneMonthMin + "-" + oneMonthMax);
                    batchOperations.add(buildBatchOperation(result.toString(), lastSymbol, oneMonthMin.toString(), oneMonthMax.toString()));
                }
            }

        } catch (JSONException e) {
            Log.e(LOG_TAG, "String to JSON failed: " + e);
        }
        return batchOperations;
    }


    public static String truncateClose(String closeValue){
/*
        String weight = closeValue.substring(0, 1);
        String ampersand = "";
        System.out.println( "tttes " + closeValue);
       // closeValue = closeValue.substring(1, closeValue.length());
        double round = (double) Math.round(Double.parseDouble(closeValue) * 100) / 100;
        closeValue = String.format("%.2f", round);
        StringBuffer changeBuffer = new StringBuffer(closeValue);
        changeBuffer.insert(0, weight);
        closeValue = changeBuffer.toString();
        System.out.println(  "tttes " + closeValue);*/
        return closeValue;
    }

    public static ContentValues buildBatchOperation(String historical, String lastSymbol, String min, String max) {
        ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(
                StockContract.BASE_CONTENT_URI);

        ContentValues contentValues = new ContentValues();
        contentValues.put(StockContract.QuoteEntry.COLUMN_SYMBOL, lastSymbol);
        contentValues.put(StockContract.QuoteEntry.COLUMN_CLOSE_HISTORICAL_ONE_MONTH, historical);
        contentValues.put(StockContract.QuoteEntry.COLUMN_MAX_MIN_ONE_MONTH, min + ";" + max);

        return contentValues;
    }



}

