package com.jnuneslab.stockholder;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jnuneslab.stockholder.data.StockContract;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;


/**
 * StockAdapter class responsible to handle all the matches information as a list
 */
public class StockAdapter extends RecyclerView.Adapter<StockAdapter.ViewHolder> implements View.OnLongClickListener{

    private static final String TAG = "CustomAdapter";
    private Cursor mCursor;
    private LayoutInflater mInflater;
    private Fragment mFragment;

    public StockAdapter(Fragment fragment) {
        mInflater = LayoutInflater.from(fragment.getActivity());
        mFragment = fragment;
    }

    public void swapCursor(Cursor newCursor) {

        mCursor = newCursor;
        notifyDataSetChanged();
    }

    public Cursor getCursor() {
        return mCursor;
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }



    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.list_item_quote, parent, false), this, parent);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Move to the current position and get the current league
        mCursor.moveToPosition(position);

        // Populate the content
        viewHolder.textViewBidPrice.setText(mCursor.getString(2));
        viewHolder.textViewSymbol.setText(mCursor.getString(1));
        viewHolder.textViewChange.setText(mCursor.getString(3));
        System.out.println(">>>>>><<" + mCursor.getString(1) + "??" + mCursor.getString(2) + "[]" + mCursor.getString(3));
        System.out.println(">>>>>><<" +mCursor.getString(6) +"??" + mCursor.getString(5) + "[]" + mCursor.getString(7) );

        String[] valuest = {"36.48","36.07","36.16","36.66","36.41","37.02","36.48","36.81","36.56","36.32","35.23","34.86","34.79","35.41","35.47","35.16","34.27","34.00","33.25","33.58","33.81"};
        String[] maxminst = {"33","37"};
        // REFACTOR
        if(mCursor.getString(6) != null) {
            valuest = mCursor.getString(6).split(",");
            maxminst = mCursor.getString(7).split(";");
        }
        viewHolder.graphicValues = valuest;
        viewHolder.minMaxValues = maxminst;
        //viewHolder.textViewBidPrice.setText("1000.00");
        //viewHolder.textViewSymbol.setText("Google");
        int sdk = Build.VERSION.SDK_INT;
        if (mCursor.getInt(mCursor.getColumnIndex("is_up")) == 1){
            if (sdk < Build.VERSION_CODES.JELLY_BEAN){
                viewHolder.textViewChange.setBackgroundDrawable(
                        mInflater.getContext().getResources().getDrawable(R.drawable.percent_change_pill));
            }else {
                viewHolder.textViewChange.setBackground(
                        mInflater.getContext().getResources().getDrawable(R.drawable.percent_change_pill));
            }
        } else {
            if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder.textViewChange.setBackgroundDrawable(
                        mInflater.getContext().getResources().getDrawable(R.drawable.percent_change_pill_red));
            } else {
                viewHolder.textViewChange.setBackground(
                        mInflater.getContext().getResources().getDrawable(R.drawable.percent_change_pill_red));
            }
        }
        if (Utils.showPercent){
            viewHolder.textViewChange.setText(mCursor.getString(mCursor.getColumnIndex("percent_change")));

        } else{

            viewHolder.textViewChange.setText(mCursor.getString(mCursor.getColumnIndex("change")));
        }
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mCursor != null ? mCursor.getCount() : 0;
    }

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView textViewBidPrice;
        protected TextView textViewSymbol;
        protected TextView textViewChange;

        protected LinearLayout graphicLayout;

        protected String[] graphicValues;
        protected String[] minMaxValues;

        protected CardView cv;
        protected boolean isDetailVisible = false;
        //public ViewHolder(View v, final Context context, final StockAdapter adapter) {
        public ViewHolder(View view, final StockAdapter adapter, final ViewGroup vg) {
            super(view);
            textViewBidPrice = (TextView) view.findViewById(R.id.bid_price);
            textViewSymbol = (TextView) view.findViewById(R.id.stock_symbol);
            textViewChange = (TextView) view.findViewById(R.id.change);
            graphicLayout = (LinearLayout) view.findViewById(R.id.linear_main2);




            cv = (CardView) view.findViewById(R.id.content_cardview);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    v.getContext().getContentResolver().delete(StockContract.QuoteEntry.CONTENT_URI,
                                            StockContract.QuoteEntry.COLUMN_SYMBOL + " = ?",
                                            new String[] {textViewSymbol.getText().toString()});
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                    Log.e("teste", "graphicLayout> " + getLayoutPosition());
                    return false;
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeDetailVisibility();

                    openChart(v, graphicValues, Float.parseFloat(minMaxValues[0]), Float.parseFloat(minMaxValues[1]));

                }
            });


        }
        private void changeDetailVisibility() {
            if(isDetailVisible) {
                graphicLayout.setVisibility(View.GONE);
                isDetailVisible = false;
            }else{
                graphicLayout.setVisibility(View.VISIBLE);
                isDetailVisible = true;
            }
        }

        }

    private View mChart;
    private String[] mMonth = new String[]{
            "Jan", "Feb", "Mar", "Feb", "Mar", "Feb", "Mar", "Feb0", "Mar1", "Feb1", "Mar2", "Feb2", "Mar3", "Feb3"
    };

    private void openChart(View view, String[] values, float min, float max){
         int[] x = { 0,1,2,3,4,5,6,7, 8, 9, 10, 11, 12,13 };
         //double[] values = {36.48,36.07,36.16,36.66,36.41,37.02,36.48,36.81,36.56,36.32,35.23,34.86,34.79,35.41,35.47,35.16,34.27,34.00,33.25,33.58,33.81};

// Creating an XYSeries for Income
        XYSeries incomeSeries = new XYSeries("Income");
// Creating an XYSeries for Expense
        XYSeries expenseSeries = new XYSeries("Expense");
// Adding data to Income and Expense Series
        for(int i=0;i<x.length;i++){
            expenseSeries.add(i,Double.parseDouble(values[i]));
        }

// Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

// Adding Expense Series to dataset
        dataset.addSeries(expenseSeries);

// Creating XYSeriesRenderer to customize expenseSeries
        XYSeriesRenderer expenseRenderer = new XYSeriesRenderer();
        expenseRenderer.setColor(ContextCompat.getColor(mInflater.getContext(), R.color.material_blue700));
        expenseRenderer.setFillPoints(true);
        expenseRenderer.setLineWidth(2f);
        expenseRenderer.setDisplayChartValues(true);
        expenseRenderer.setDisplayChartValuesDistance(10);
        expenseRenderer.setChartValuesTextSize(30);
//setting line graph point style to circle
        expenseRenderer.setPointStyle(PointStyle.SQUARE);
//setting stroke of the line chart to solid
        expenseRenderer.setStroke(BasicStroke.SOLID);

// Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(3);
       // multiRenderer.setChartTitle("Income vs Expense Chart");
       // multiRenderer.setXTitle("Year 2014");
       // multiRenderer.setYTitle("Amount in Dollars");

/***
 * Customizing graphs
 */
//setting text size of the title
        multiRenderer.setChartTitleTextSize(28);
//setting text size of the axis title
        multiRenderer.setAxisTitleTextSize(24);
//setting text size of the graph lable
        multiRenderer.setLabelsTextSize(24);
//setting zoom buttons visiblity
        multiRenderer.setZoomButtonsVisible(false);
//setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(false, false);
//setting click false on graph
        multiRenderer.setClickEnabled(false);
//setting zoom to false on both axis
        multiRenderer.setZoomEnabled(false, false);
//setting lines to display on y axis
        multiRenderer.setShowGridY(true);
//setting lines to display on x axis
        multiRenderer.setShowGridX(true);
//setting legend to fit the screen size
        multiRenderer.setFitLegend(true);
//setting displaying line on grid
        multiRenderer.setShowGrid(true);
//setting zoom to false
        multiRenderer.setZoomEnabled(false);
//setting external zoom functions to false
        multiRenderer.setExternalZoomEnabled(false);
//setting displaying lines on graph to be formatted(like using graphics)
        multiRenderer.setAntialiasing(true);
//setting to in scroll to false
        multiRenderer.setInScroll(false);
//setting to set legend height of the graph
        multiRenderer.setLegendHeight(30);
//setting x axis label align
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
//setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
//setting text style
        multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
//setting no of values to display in y axis
        multiRenderer.setYLabels(5);
// setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 4000.
// if you use dynamic values then get the max y value and set here
        multiRenderer.setYAxisMax(max);
        multiRenderer.setYAxisMin(min);
//setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMin(-0.5);
//setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMax(14);
//setting bar size or space between two bars
//multiRenderer.setBarSpacing(0.5);
//Setting background color of the graph to transparent
        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
//Setting margin color of the graph to transparent
        multiRenderer.setMarginsColor(mInflater.getContext().getResources().getColor(R.color.transparent_background));
        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setScale(2f);
//setting x axis point size
        multiRenderer.setPointSize(4f);
//setting the margin size for the graph in the order top, left, bottom, right
        multiRenderer.setMargins(new int[]{30, 30, 30, 30});

        for(int i=0; i< x.length;i++){
            multiRenderer.addXTextLabel(i, mMonth[i]);
        }

// Adding incomeRenderer and expenseRenderer to multipleRenderer
// Note: The order of adding dataseries to dataset and renderers to multipleRenderer
// should be same
        multiRenderer.addSeriesRenderer(expenseRenderer);

//this part is used to display graph on the xml
        LinearLayout chartContainer = (LinearLayout) view.findViewById(R.id.linear_main2);
        LinearLayout.LayoutParams test = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 500);
        chartContainer.setLayoutParams(test);
//remove any views before u paint the chart
       // chartContainer.removeAllViews();
//drawing bar chart
        mChart = ChartFactory.getLineChartView(view.getContext(), dataset, multiRenderer);
//adding the view to the linearlayout
        chartContainer.addView(mChart);

    }
}