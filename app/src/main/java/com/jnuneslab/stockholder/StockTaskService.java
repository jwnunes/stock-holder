package com.jnuneslab.stockholder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.jnuneslab.stockholder.data.StockContract;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

//http://chart.finance.yahoo.com/z?s=AAPL&t=1d&q=l&l=on&z=s&z=s/

//http://trading.cheno.net/downloading-google-intraday-historical-data-with-python/
/**
 * Created by sam_chordas on 9/30/15.
 * The GCMTask service is primarily for periodic tasks. However, OnRunTask can be called directly
 * and is used for the initialization and adding task as well.
 */
public class StockTaskService extends GcmTaskService {
    private String LOG_TAG = StockTaskService.class.getSimpleName();

    private OkHttpClient client = new OkHttpClient();
    private Context mContext;
    private StringBuilder mStoredSymbols = new StringBuilder();
    private boolean isUpdate;

    public StockTaskService() {
    }

    public StockTaskService(Context context) {
        mContext = context;
    }

    String fetchData(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    public int onRunTask(TaskParams params) {
        Cursor initQueryCursor;
        if (mContext == null) {
            mContext = this;
        }
        StringBuilder urlStringBuilder = new StringBuilder();
        StringBuilder urlStringBuilderHistorical = new StringBuilder();
        try {
            // Base URL for the Yahoo query
            urlStringBuilder.append("https://query.yahooapis.com/v1/public/yql?q=");
            urlStringBuilderHistorical.append("https://query.yahooapis.com/v1/public/yql?q=");
            // select * from yahoo.finance.historicaldata where symbol = "YHOO" and startDate = "2009-09-11" and endDate = "2010-03-10"
            urlStringBuilder.append(URLEncoder.encode("select * from yahoo.finance.quotes where symbol in (", "UTF-8"));
            urlStringBuilderHistorical.append(URLEncoder.encode("select * from yahoo.finance.historicaldata where symbol in (", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (params.getTag().equals("init") || params.getTag().equals("periodic")) {
            isUpdate = true;
            initQueryCursor = mContext.getContentResolver().query(StockContract.BASE_CONTENT_URI,
            new String[]{"Distinct " + StockContract.QuoteEntry.COLUMN_SYMBOL}, null,
            null, null);
            if (initQueryCursor.getCount() == 0 || initQueryCursor == null) {
                // Init task. Populates DB with quotes for the symbols seen below
                try {
                    urlStringBuilder.append(
                            URLEncoder.encode("\"YHOO\",\"AAPL\",\"GOOG\",\"MSFT\")", "UTF-8"));
                    urlStringBuilderHistorical.append(
                            URLEncoder.encode("\"YHOO\",\"AAPL\",\"GOOG\",\"MSFT\")", "UTF-8"));
                    urlStringBuilderHistorical.append(
                            URLEncoder.encode(" and startDate = \"2016-03-11\" and endDate = \"2016-04-11\" ", "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else if (initQueryCursor != null) {
                DatabaseUtils.dumpCursor(initQueryCursor);
                initQueryCursor.moveToFirst();
                for (int i = 0; i < initQueryCursor.getCount(); i++) {
                    mStoredSymbols.append("\"" +
                            initQueryCursor.getString(initQueryCursor.getColumnIndex("symbol")) + "\",");
                    initQueryCursor.moveToNext();
                }
                mStoredSymbols.replace(mStoredSymbols.length() - 1, mStoredSymbols.length(), ")");
                try {
                    urlStringBuilder.append(URLEncoder.encode(mStoredSymbols.toString(), "UTF-8"));
                    urlStringBuilderHistorical.append(URLEncoder.encode(mStoredSymbols.toString(), "UTF-8"));
                    urlStringBuilderHistorical.append(
                            URLEncoder.encode(" and startDate = \"2016-03-11\" and endDate = \"2016-04-11\" ", "UTF-8"));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } else if (params.getTag().equals("add")) {
            isUpdate = false;
            // get symbol from params.getExtra and build query
            String stockInput = params.getExtras().getString("symbol");
            try {
                urlStringBuilder.append(URLEncoder.encode("\"" + stockInput + "\")", "UTF-8"));
                urlStringBuilderHistorical.append(URLEncoder.encode("\"" + stockInput + "\")", "UTF-8"));
                urlStringBuilderHistorical.append(
                        URLEncoder.encode(" and startDate = \"2016-03-11\" and endDate = \"2016-04-11\" ", "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        // finalize the URL for the API query.
        urlStringBuilder.append("&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables."
                + "org%2Falltableswithkeys&callback=");
        urlStringBuilderHistorical.append("&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables."
                + "org%2Falltableswithkeys&callback=");

        String urlString;
        String urlStringHistorical;
        String getResponse;
        String getResponseHistorical;
        int result = GcmNetworkManager.RESULT_FAILURE;

        if (urlStringBuilder != null) {
            urlString = urlStringBuilder.toString();
            urlStringHistorical = urlStringBuilderHistorical.toString();
            try {
                Log.e("teste", "teste2<<<" + urlString);
                Log.e("teste", "teste2<<<" + urlStringHistorical);
                getResponse = fetchData(urlString);
                getResponseHistorical = fetchData(urlStringHistorical);
                result = GcmNetworkManager.RESULT_SUCCESS;
                try {
                    ContentValues contentValues = new ContentValues();
                    ArrayList<ContentValues> responseList = new ArrayList<ContentValues>();
                    // update ISCURRENT to 0 (false) so new data is current
                    if (isUpdate) {
                        contentValues.put(StockContract.QuoteEntry.COLUMN_IS_CURRENT, 0);
                        mContext.getContentResolver().update(StockContract.BASE_CONTENT_URI, contentValues,
                               null, null);
                    }
                    //Vector<ContentValues> values = new Vector<>(10);

                    responseList = Utils.quoteJsonToContentVals(getResponse);
                    Log.e("teste", "teste<<<" + getResponse);
                    Log.e("teste", "teste<<<" + getResponseHistorical);
                    ContentValues[] insert_data = new ContentValues[responseList.size()];
                    responseList.toArray(insert_data);
                    //values.toArray(insert_data);

                    System.out.println("<>> " + urlString + "<>" + insert_data);
                    System.out.println("<>> " + urlStringBuilderHistorical.toString());
                    mContext.getContentResolver().bulkInsert(
                            StockContract.BASE_CONTENT_URI, insert_data);

                    responseList = Utils2.quoteJsonToContentVals(getResponseHistorical);
                    insert_data = new ContentValues[responseList.size()];
                    responseList.toArray(insert_data);

                    for(ContentValues value : insert_data){

                        System.out.println("<key "  + value.getAsString(StockContract.QuoteEntry.COLUMN_BID_PRICE) + "//"  +value.getAsString(StockContract.QuoteEntry.COLUMN_CHANGE));
                         if(!value.getAsString(StockContract.QuoteEntry.COLUMN_CLOSE_HISTORICAL_ONE_MONTH).equals("null")) {

                            mContext.getContentResolver().update(
                                    StockContract.BASE_CONTENT_URI,
                                    value,
                                    StockContract.QuoteEntry.COLUMN_SYMBOL + " = ?",
                                    new String[]{value.getAsString(StockContract.QuoteEntry.COLUMN_SYMBOL)});
                        }
                    }
                   // bulkInsert(StockContract.CONTENT_AUTHORITY,
                  //          Utils.quoteJsonToContentVals(getResponse));
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Error applying batch insert", e);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

}
